<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <!-- vamos a crear formulario: los botones hay dos tipos:input(sin submit) y button(submit por defecto)*/
        /*en la etiqueta form se utilizan dos atributos: uno el method y el otro es el
        action(es el enlace para enviarlo a otro php envia a otro php)
        dentro del method hay el get(viajan en la url) y el post(viajan en el cuerpo)*/ -->
    </head>
    <body>
        
        <form action="8.php" method="get">
            <label for="inombre">Nombre del alumno</label>
            <input type="text" id="inombre" name="nombre">
            <label for="inumero">Numero1</label>
            <input type="number" id="numero" name="numero[]">
            <label for="inumero2">Numero2</label>
            <input type="number" id="numero" name="numero[]">
            <div>
                <label for="ipotes">Potes</label>
                <input id="ipotes" type="checkbox" name="poblacion[]" value="Potes"/>
                <label for="isantander">Santander</label>
                  <input id="isantander" type="checkbox" name="poblacion[]" value="Santander"/>
            </div>
            <div>
            <label for="iRojo">Rojo</label>
            <input id="iRojo" type="radio" name="colores" value="R"/>
            <label for="iAzul">Azul</label>
            <input id="iAzul" type="radio" name="colores" value="A"/>
            </div>
            <div>
            <label for="">Selecciona nombres</label>
            <select name="nombres[]" id="inombres" multiple>
            <option values="0">Roberto</option>
            <option values="1">Silvia</option>
            <option values="2">Laura</option> 
            </select>
            </div>  
            <button>Enviar</button>
        </form>
        <?php
        ?>
    </body>
</html>
